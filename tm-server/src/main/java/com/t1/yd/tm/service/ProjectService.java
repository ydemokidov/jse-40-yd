package com.t1.yd.tm.service;

import com.t1.yd.tm.api.repository.IProjectRepository;
import com.t1.yd.tm.api.service.IConnectionService;
import com.t1.yd.tm.api.service.IProjectService;
import com.t1.yd.tm.enumerated.Status;
import com.t1.yd.tm.exception.entity.ProjectNotFoundException;
import com.t1.yd.tm.exception.field.*;
import com.t1.yd.tm.model.Project;
import lombok.SneakyThrows;
import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public final class ProjectService extends AbstractUserOwnedService<Project, IProjectRepository> implements IProjectService {

    public ProjectService(@NotNull IConnectionService connectionService) {
        super(connectionService);
    }

    @NotNull
    @Override
    public Project findProjectById(@NotNull String id) {
        @Nullable final Project project = findOneById(id);
        if (project == null) throw new ProjectNotFoundException();
        return project;
    }

    @NotNull
    public Project findProjectByIndex(@NotNull final String userId, @NotNull final Integer index) {
        @Nullable final Project project = findOneByIndex(userId, index);
        if (project == null) throw new ProjectNotFoundException();
        return project;
    }

    @NotNull
    @Override
    public Project findProjectById(@NotNull final String userId, @NotNull final String id) {
        @Nullable final Project project = findOneById(userId, id);
        if (project == null) throw new ProjectNotFoundException();
        return project;
    }

    @NotNull
    @Override
    public Project create(@NotNull final String userId, @NotNull final String name, @NotNull final String description) {
        if (name.isEmpty()) throw new NameEmptyException();
        if (description.isEmpty()) throw new DescriptionEmptyException();
        return add(new Project(userId, name, description));
    }

    @NotNull
    @Override
    @SneakyThrows
    public Project updateById(@NotNull final String userId, @NotNull final String id, @NotNull final String name, @NotNull final String description) {
        if (userId.isEmpty()) throw new UserIdEmptyException();
        if (id.isEmpty()) throw new IdEmptyException();
        if (name.isEmpty()) throw new NameEmptyException();

        @Nullable final Project project = findOneById(userId, id);
        if (project == null) throw new ProjectNotFoundException();

        project.setName(name);
        project.setDescription(description);
        update(project);

        return project;
    }

    @NotNull
    @Override
    @SneakyThrows
    public Project updateByIndex(@NotNull final String userId, @NotNull final Integer index, @NotNull final String name, @NotNull final String description) {
        if (userId.isEmpty()) throw new UserIdEmptyException();
        if (index < 0) throw new IndexIncorrectException();
        if (name.isEmpty()) throw new NameEmptyException();

        @Nullable final Project project = findOneByIndex(userId, index);
        if (project == null) throw new ProjectNotFoundException();

        project.setName(name);
        project.setDescription(description);
        update(project);

        return project;
    }

    @NotNull
    @Override
    @SneakyThrows
    public Project changeStatusById(@NotNull final String userId, @NotNull final String id, @NotNull final Status status) {
        if (userId.isEmpty()) throw new UserIdEmptyException();
        if (id.isEmpty()) throw new IdEmptyException();

        @Nullable final Project project = findOneById(userId, id);
        if (project == null) throw new ProjectNotFoundException();

        project.setStatus(status);

        update(project);
        return project;
    }

    @NotNull
    @Override
    @SneakyThrows
    public Project changeStatusByIndex(@NotNull final String userId, @NotNull final Integer index, @NotNull final Status status) {
        if (userId.isEmpty()) throw new UserIdEmptyException();
        if (index < 0 || index > findAll(userId).size()) throw new IndexIncorrectException();
        @Nullable final Project project = findOneByIndex(userId, index);
        if (project == null) throw new ProjectNotFoundException();

        project.setStatus(status);
        update(project);

        return project;
    }

    @NotNull
    @SneakyThrows
    public Project removeProjectById(@NotNull final String userId, @NotNull final String id) {
        if (userId.isEmpty()) throw new UserIdEmptyException();
        if (id.isEmpty()) throw new IdEmptyException();

        @Nullable Project project = removeById(userId, id);
        if (project == null) throw new ProjectNotFoundException();
        return project;
    }

    @NotNull
    @SneakyThrows
    public Project removeProjectByIndex(@NotNull final String userId, @NotNull final Integer index) {
        @Nullable final Project project = removeByIndex(userId, index);
        if (project == null) throw new ProjectNotFoundException();
        return project;
    }

    @NotNull
    @Override
    protected IProjectRepository getRepository(@NotNull final SqlSession sqlSession) {
        return sqlSession.getMapper(IProjectRepository.class);
    }

}