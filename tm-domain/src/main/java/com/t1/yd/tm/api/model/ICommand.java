package com.t1.yd.tm.api.model;

import com.t1.yd.tm.enumerated.Role;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public interface ICommand {

    @Nullable
    String getArgument();

    @NotNull
    String getDescription();

    @NotNull
    String getName();

    void execute();

    @Nullable
    Role[] getRoles();

}
