package com.t1.yd.tm.service;

import com.t1.yd.tm.api.repository.IProjectRepository;
import com.t1.yd.tm.api.repository.ITaskRepository;
import com.t1.yd.tm.api.service.IConnectionService;
import com.t1.yd.tm.api.service.IProjectTaskService;
import com.t1.yd.tm.exception.entity.ProjectNotFoundException;
import com.t1.yd.tm.exception.entity.TaskNotFoundException;
import com.t1.yd.tm.exception.field.ProjectIdEmptyException;
import com.t1.yd.tm.exception.field.TaskIdEmptyException;
import com.t1.yd.tm.exception.field.UserIdEmptyException;
import com.t1.yd.tm.model.Task;
import lombok.AllArgsConstructor;
import lombok.SneakyThrows;
import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;

@AllArgsConstructor
public final class ProjectTaskService implements IProjectTaskService {

    @NotNull
    private final IConnectionService connectionService;


    @NotNull
    private IProjectRepository getProjectRepository(@NotNull final SqlSession sqlSession) {
        return sqlSession.getMapper(IProjectRepository.class);
    }

    @NotNull
    private ITaskRepository getTaskRepository(@NotNull final SqlSession sqlSession) {
        return sqlSession.getMapper(ITaskRepository.class);
    }


    @Override
    @SneakyThrows
    public void bindTaskToProject(@NotNull final String userId, @NotNull final String taskId, @NotNull final String projectId) {
        if (projectId.isEmpty()) throw new ProjectIdEmptyException();
        if (taskId.isEmpty()) throw new TaskIdEmptyException();
        if (userId.isEmpty()) throw new UserIdEmptyException();

        @NotNull final SqlSession sqlSession = connectionService.openSession();

        try {
            @NotNull final IProjectRepository projectRepository = getProjectRepository(sqlSession);
            if (projectRepository.findOneByIdWithUserId(userId, projectId) == null)
                throw new ProjectNotFoundException();
            @NotNull final ITaskRepository taskRepository = getTaskRepository(sqlSession);
            @Nullable final Task task = taskRepository.findOneByIdWithUserId(userId, taskId);
            if (task == null) throw new TaskNotFoundException();

            task.setProjectId(projectId);

            taskRepository.update(task);

            sqlSession.commit();
        } catch (Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Override
    @SneakyThrows
    public void removeProjectById(@NotNull final String userId, @NotNull final String projectId) {
        if (userId.isEmpty()) throw new UserIdEmptyException();
        if (projectId.isEmpty()) throw new ProjectIdEmptyException();

        @NotNull SqlSession sqlSession = connectionService.openSession();
        try {
            @NotNull final IProjectRepository projectRepository = getProjectRepository(sqlSession);
            if (projectRepository.findOneByIdWithUserId(userId, projectId) == null)
                throw new ProjectNotFoundException();
            @NotNull final ITaskRepository taskRepository = getTaskRepository(sqlSession);
            @NotNull final List<Task> tasks = taskRepository.findAllByProjectId(userId, projectId);
            for (@NotNull final Task task : tasks) {
                taskRepository.removeById(task.getId());
            }
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Override
    @SneakyThrows
    public void unbindTaskFromProject(@NotNull final String userId, @NotNull final String taskId, @NotNull final String projectId) {
        if (projectId.isEmpty()) throw new ProjectIdEmptyException();
        if (taskId.isEmpty()) throw new TaskIdEmptyException();
        if (userId.isEmpty()) throw new UserIdEmptyException();

        @NotNull final SqlSession sqlSession = connectionService.openSession();
        try {
            @NotNull final IProjectRepository projectRepository = getProjectRepository(sqlSession);
            if (projectRepository.findOneByIdWithUserId(userId, projectId) == null)
                throw new ProjectNotFoundException();

            @NotNull final ITaskRepository taskRepository = getTaskRepository(sqlSession);
            @Nullable final Task task = taskRepository.findOneByIdWithUserId(userId, taskId);
            if (task == null) throw new TaskNotFoundException();

            task.setProjectId(null);

            taskRepository.update(task);
            sqlSession.commit();
        } catch (Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

}
