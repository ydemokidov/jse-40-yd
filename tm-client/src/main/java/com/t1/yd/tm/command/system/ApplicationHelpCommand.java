package com.t1.yd.tm.command.system;

import com.t1.yd.tm.api.model.ICommand;
import com.t1.yd.tm.command.AbstractCommand;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public final class ApplicationHelpCommand extends AbstractSystemCommand {

    @NotNull
    public static final String NAME = "help";

    @NotNull
    public static final String ARGUMENT = "-h";

    @NotNull
    public static final String DESCRIPTION = "Show info about program";

    @Override
    public void execute() {
        System.out.println("[HELP]");
        for (String commandHelp : getHelpStrings()) {
            System.out.println(commandHelp);
        }
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    public List<String> getHelpStrings() {
        List<String> helpStrings = new ArrayList<>();
        final Collection<AbstractCommand> commands = getCommandService().getTerminalCommands();
        for (final ICommand command : commands) helpStrings.add(command.toString());
        return helpStrings;
    }
}
