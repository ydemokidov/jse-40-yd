package com.t1.yd.tm.api.endpoint;

import com.t1.yd.tm.dto.request.user.*;
import com.t1.yd.tm.dto.response.user.*;
import org.jetbrains.annotations.NotNull;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService
public interface IUserEndpoint extends IEndpoint {

    @NotNull
    String NAME = "UserEndpoint";

    @NotNull
    String PART = NAME + "Service";

    @WebMethod(exclude = true)
    static IUserEndpoint newInstance() {
        return newInstance(HOST, PORT);
    }

    @WebMethod(exclude = true)
    static IUserEndpoint newInstance(@NotNull IConnectionProvider connectionProvider) {
        @NotNull final String host = connectionProvider.getHost();
        @NotNull final String port = connectionProvider.getPort();
        return IEndpoint.newInstance(host, port, NAME, SPACE, PART, IUserEndpoint.class);
    }

    @WebMethod(exclude = true)
    static IUserEndpoint newInstance(@NotNull final String host, @NotNull final String port) {
        return IEndpoint.newInstance(host, port, NAME, SPACE, PART, IUserEndpoint.class);
    }

    @NotNull
    @WebMethod
    UserPasswordChangeResponse passwordChange(@WebParam(name = REQUEST, partName = REQUEST) @NotNull UserPasswordChangeRequest request);

    @NotNull
    @WebMethod
    UserLockResponse lock(@WebParam(name = REQUEST, partName = REQUEST) @NotNull UserLockRequest request);

    @NotNull
    @WebMethod
    UserUnlockResponse unlock(@WebParam(name = REQUEST, partName = REQUEST) @NotNull UserUnlockRequest request);

    @NotNull
    @WebMethod
    UserRegistryResponse registry(@WebParam(name = REQUEST, partName = REQUEST) @NotNull UserRegistryRequest request);

    @NotNull
    @WebMethod
    UserRemoveResponse remove(@WebParam(name = REQUEST, partName = REQUEST) @NotNull UserRemoveRequest request);

    @NotNull
    @WebMethod
    UserUpdateProfileResponse updateProfile(@WebParam(name = REQUEST, partName = REQUEST) @NotNull UserUpdateProfileRequest request);

}
