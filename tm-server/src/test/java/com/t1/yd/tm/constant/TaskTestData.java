package com.t1.yd.tm.constant;

import com.t1.yd.tm.model.Task;
import lombok.experimental.UtilityClass;
import org.jetbrains.annotations.NotNull;

import java.util.Arrays;
import java.util.List;

@UtilityClass
public class TaskTestData {

    @NotNull
    public static final Task USER1_PROJECT1_TASK1 = new Task();

    @NotNull
    public static final Task USER1_PROJECT1_TASK2 = new Task();

    @NotNull
    public static final Task ADMIN_PROJECT1_TASK1 = new Task();

    @NotNull
    public static final Task ADMIN_PROJECT_TASK2 = new Task();

    @NotNull
    public final static String TASK_NAME = "task1 name";

    @NotNull
    public final static String TASK_DESCRIPTION = "task1 desc";

    @NotNull
    public static List<Task> ALL_TASKS = Arrays.asList(USER1_PROJECT1_TASK1, USER1_PROJECT1_TASK2, ADMIN_PROJECT1_TASK1, ADMIN_PROJECT_TASK2);

    @NotNull
    public static List<Task> USER1_PROJECT1_TASKS = Arrays.asList(USER1_PROJECT1_TASK1, USER1_PROJECT1_TASK2);

}
