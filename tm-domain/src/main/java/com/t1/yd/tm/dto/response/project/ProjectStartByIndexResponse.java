package com.t1.yd.tm.dto.response.project;

import com.t1.yd.tm.model.Project;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;

@NoArgsConstructor
public class ProjectStartByIndexResponse extends AbstractProjectResponse {

    public ProjectStartByIndexResponse(@Nullable final Project project) {
        super(project);
    }

    public ProjectStartByIndexResponse(@Nullable final Throwable throwable) {
        super(throwable);
    }

}
